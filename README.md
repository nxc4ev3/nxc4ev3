# NXC4EV3 - Run your NXC programs on the EV3!

This software allows you to run mostly unmodified NXC sources on the EV3 brick. It is built from these modules:

* NXC2CC - transcompiler from NXC to C
* STDNXC - wrapper around EV3-API
* NXC4EV3-GUI - user-friendly graphical interface

Without these third-party software this software wouldn't exist:
* [EV3-API](https://github.com/c4ev3/EV3-API)   - C implementation of a basic NXC-like API on the EV3
* [ev3duder](https://github.com/c4ev3/ev3duder) - EV3 UploaDER
* [GCC for EV3](https://sourcery.mentor.com/) - compiler from the generated C source to an ELF binary that runs on the EV3
* [EV3 firmware source code](https://mi-od-live-s.legocdn.com/r/www/r/mindstorms/-/media/franchises/mindstorms%202014/downloads/firmware%20and%20software/advanced/lms2012v109dtar.bz2?l.r2=1674223888)
